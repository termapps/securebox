/*
 * Copyright (C) 2017-2019 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class UserAccount {
    public static final String LOGIN_NAME_REGEX = "[\\p{Lower}_][\\p{Lower}\\d_-]*[$]?";
    public static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*\\p{Lower})(?=.*\\p{Upper})(?=.*\\p{Punct}).{5,20})";

    private final static String DISABLED_ACCOUNT = "*";
    private final static String separator = ":";

    private final File passfile;

    private String login_name = null;
    private String encrypted_password = null;
    private String user_id = null;
    private String group_id = null;
    private String comment = null;
    private String home_directory = null;
    private String command_interpreter = null;


    private UserAccount(File etcdir) {
        passfile = new File(etcdir, "passwd");
    }

    public static void initialize(File etcdir) {
        UserAccount ua = new UserAccount(etcdir);
        if (!ua.passfile.exists()) {
            // initialize with default values
            ua.login_name = "securebox";
            ua.encrypted_password = DISABLED_ACCOUNT;
            ua.home_directory = "$HOME";
            ua.command_interpreter = "$SHELL";
            // store default user account
            ua.write();
        }
        // noinspection SetWorldReadable
        ua.passfile.setReadable(true, false);
    }

    public static UserAccount load(File etcdir) {
        UserAccount ua = new UserAccount(etcdir);

        String line;
        try {
            BufferedReader in = new BufferedReader(new FileReader(ua.passfile));
            line = in.readLine();
            in.close();
            if (line == null) return ua;
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return ua;
        }

        String[] split = line.split(separator, -1);
        if (split.length == 7) {
            ua.login_name = split[0];
            ua.encrypted_password = split[1];
            ua.user_id = split[2];
            ua.group_id = split[3];
            ua.comment = split[4];
            ua.home_directory = split[5];
            ua.command_interpreter = split[6];
        }

        return ua;
    }

    public boolean write() {
        try {
            PrintWriter out = new PrintWriter(passfile);
            out.println(this);
            out.flush();
            out.close();
            return true;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }


    public String toString() {
        StringBuilder b = new StringBuilder();

        if (login_name != null) b.append(login_name);
        b.append(separator);

        if (encrypted_password != null) b.append(encrypted_password);
        b.append(separator);

        if (user_id != null) b.append(user_id);
        b.append(separator);

        if (group_id != null) b.append(group_id);
        b.append(separator);

        if (comment != null) b.append(comment);
        b.append(separator);

        if (home_directory != null) b.append(home_directory);
        b.append(separator);

        if (command_interpreter != null) b.append(command_interpreter);

        return b.toString();
    }


    public String getLoginName() {
        return login_name;
    }

    public boolean setLoginName(String value) {
        if (!value.matches(LOGIN_NAME_REGEX))
            return false;

        login_name = value;
        return true;
    }


    public boolean isLoginEnabled() {
        return !DISABLED_ACCOUNT.equals(encrypted_password);
    }

    public boolean disableLogin() {
        encrypted_password = DISABLED_ACCOUNT;
        return true;
    }


    public boolean setPassword(String value) {
        if (!value.matches(PASSWORD_REGEX))
            return false;

        encrypted_password = com.termoneplus.crypto.MD5Crypt.crypt(value);
        return true;
    }


    public String getHomeDirectory() {
        return home_directory;
    }

    public boolean setHomeDirectory(String value) {
        home_directory = value;
        return true;
    }


    public String getCommandInterpreter() {
        return command_interpreter;
    }

    public boolean setCommandInterpreter(String value) {
        command_interpreter = value;
        return true;
    }
}
