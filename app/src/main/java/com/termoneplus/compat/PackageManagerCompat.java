/*
 * Copyright (C) 2022 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.compat;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.RequiresApi;


public class PackageManagerCompat {

    public static PackageInfo getPackageInfo(PackageManager pm, String name)
            throws PackageManager.NameNotFoundException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU /*API level 33*/)
            return Compat33.getPackageInfo(pm, name);
        else
            return Compat1.getPackageInfo(pm, name);
    }

    @RequiresApi(33)
    private static class Compat33 {
        private static PackageInfo getPackageInfo(PackageManager pm, String name)
                throws PackageManager.NameNotFoundException {
            PackageManager.PackageInfoFlags flags = PackageManager.PackageInfoFlags.of(0);
            return pm.getPackageInfo(name, flags);
        }
    }

    private static class Compat1 {
        // Explicitly suppress deprecation warnings
        // "getPackageInfo(String,int) in PackageManager has been deprecated"
        @SuppressWarnings({"deprecation", "RedundantSuppression"})
        private static PackageInfo getPackageInfo(PackageManager pm, String name)
                throws PackageManager.NameNotFoundException {
            return pm.getPackageInfo(name, 0);
        }
    }
}
