/*
 * Copyright (C) 2019-2022 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.compat;

import android.content.pm.PackageInfo;
import android.os.Build;

import androidx.annotation.RequiresApi;


public class PackageInfoCompat {

    public static long getVersion(PackageInfo pkg_info) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P /* API Level 28 */)
            return Compat28.getVersion(pkg_info);
        else
            return Compat1.getVersion(pkg_info);
    }

    @RequiresApi(28)
    private static class Compat28 {
        private static long getVersion(PackageInfo pkg_info) {
            return pkg_info.getLongVersionCode();
        }
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    private static class Compat1 {
        private static long getVersion(PackageInfo pkg_info) {
            return pkg_info.versionCode;
        }
    }
}
