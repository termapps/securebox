/*
 * Copyright (C) 2024 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;

import com.termoneplus.securebox.Application;
import com.termoneplus.v1.ICommand;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


public class CommandService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new RBinder();
    }

    private static final class RBinder extends ICommand.Stub {
        public String[] getCommands() {
            return new String[]{
                    "ssh", "ssh-keygen",
                    "sftp", "scp", "ssh-keyscan",
                    "ssh-agent", "ssh-add",
                    "openssl",
                    // Notes:
                    // - return daemon only for backward compatibility
                    // - from security point of view application should run
                    //   daemon but when "shared user id" is not used.
                    "sshd"
            };
        }

        @Override
        public String getPath(String cmd) {
            for (String item : getCommands()) {
                if (item.equals(cmd)) {
                    String name = "libcmd-" + cmd + ".so";
                    File exe = new File(Application.xbindir, name);
                    return exe.getPath();
                }
            }
            return null;
        }

        @Override
        public String[] getEnvironment(String cmd) {
            if ("openssl".equals(cmd))
                return getEnvironmentOpenSSL();

            return new String[0];
        }

        @Override
        public ParcelFileDescriptor openConfiguration(String path) {
            if ("/etc/passwd".equals(path))
                return open_sysconfig("passwd");
            if ("/etc/ssh_config".equals(path))
                return open_sysconfig("ssh_config");
            if ("/etc/openssl.cnf".equals(path))
                return open_sysconfig("openssl.cnf");
            if ("/etc/sshd_config".equals(path))
                return open_sysconfig("sshd_config");
            return null;
        }
    }

    private static ParcelFileDescriptor open_sysconfig(String name) {
        try {
            File conf = new File(Application.etcdir.getPath(), name);
            try (FileInputStream in = new FileInputStream(conf)) {
                FileDescriptor fd = in.getFD();
                return ParcelFileDescriptor.dup(fd);
            }
        } catch (FileNotFoundException ignore) {
        } catch (IOException ignore) {
        }
        return null;
    }

    private static String[] getEnvironmentOpenSSL() {
        ArrayList<String> env = new ArrayList<>();

        String cmd_conf = new File(Application.etcdir, "openssl.cnf").getPath();
        env.add("OPENSSL_CONF=" + cmd_conf);
        return env.toArray(new String[0]);
    }
}
