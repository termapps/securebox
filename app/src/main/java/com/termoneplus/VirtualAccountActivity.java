/*
 * Copyright (C) 2017-2019 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.termoneplus.securebox.Application;
import com.termoneplus.securebox.R;
import com.termoneplus.widget.ScreenMessage;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class VirtualAccountActivity extends AppCompatActivity {
    private UserAccount account;

    private EditText account_user;
    private CheckBox account_enable;
    private EditText account_password;
    private EditText account_home;
    private EditText account_shell;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_account);

        {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        }
        {    // Show the Up button in the action bar.
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null)
                actionBar.setDisplayHomeAsUpEnabled(true);
        }

        account = UserAccount.load(Application.etcdir);

        account_user = findViewById(R.id.account_user);
        if (account.getLoginName() != null)
            account_user.setText(account.getLoginName());

        account_enable = findViewById(R.id.account_enable);
        account_password = findViewById(R.id.account_password);
        {
            boolean enabled = account.isLoginEnabled();
            account_enable.setChecked(enabled);
            account_password.setEnabled(enabled);
        }
        account_enable.setOnCheckedChangeListener((buttonView, isChecked) -> account_password.setEnabled(isChecked));

        account_home = findViewById(R.id.account_home);
        if (account.getHomeDirectory() != null)
            account_home.setText(account.getHomeDirectory());

        account_shell = findViewById(R.id.account_shell);
        if (account.getCommandInterpreter() != null)
            account_shell.setText(account.getCommandInterpreter());

        FloatingActionButton save = findViewById(R.id.save);
        if (account.getLoginName() == null)
            save.setEnabled(false);
        else
            save.setOnClickListener(view -> onAccountSave());
    }

    private void onAccountSave() {
        String value;

        value = account_user.getText().toString();
        if (!account.setLoginName(value)) {
            String error = getResources().getString(R.string.account_user_invalid);
            account_user.setError(error);
            return;
        }

        if (account_enable.isChecked()) {
            value = account_password.getText().toString();
            if (!account.setPassword(value)) {
                String error = getResources().getString(R.string.account_password_invalid);
                account_password.setError(error);
                return;
            }
        } else
            account.disableLogin();

        value = account_home.getText().toString();
        if (!account.setHomeDirectory(value)) {
            return;
        }

        value = account_shell.getText().toString();
        if (!account.setCommandInterpreter(value)) {
            return;
        }

        if (account.write())
            ScreenMessage.show(getApplicationContext(), R.string.account_saved);
    }
}
