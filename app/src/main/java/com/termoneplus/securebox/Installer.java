/*
 * Copyright (C) 2019-2024 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.securebox;

import com.termoneplus.compat.FilesCompat;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;


public class Installer {

    public static boolean install_directory(File dir, boolean share) {
        if (!(dir.exists() || dir.mkdir())) return false;

        // always preset directory permissions
        return dir.setReadable(true, !share) &&
                dir.setExecutable(true, false);
    }

    public static boolean copy_executable(File source, File target_path) {
        int buflen = 32 * 1024; // 32k
        byte[] buf = new byte[buflen];

        File target = new File(target_path, source.getName());
        File backup = new File(target.getAbsolutePath() + "-bak");
        if (target.exists())
            if (!target.renameTo(backup))
                return false;

        try {
            OutputStream os = FilesCompat.newOutputStream(target);
            InputStream is = FilesCompat.newInputStream(source);
            int len;
            while ((len = is.read(buf, 0, buflen)) > 0) {
                os.write(buf, 0, len);
            }
            os.close();
            is.close();

            if (backup.exists())
                deleteFile(backup);

            // always preset executable permissions
            return target.setReadable(true) &&
                    target.setExecutable(true, false);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public static boolean install_xcommands(File target_path) {
        final Pattern pattern = Pattern.compile("libcmd-(.*).so");
        File[] list = Application.libdir.listFiles((dir, name) -> pattern.matcher(name).find());
        if (list != null) {
            for (File exe : list) {
                if (!Installer.copy_executable(exe, target_path))
                    return false;
            }
        }
        return true;
    }

    public static void clean_directory(File dir) {
        if (!dir.exists()) return;

        String[] list = dir.list();
        if (list != null)
            for (String item : list)
                deleteFile(new File(dir, item));
        deleteFile(dir);
    }

    private static void deleteFile (File file) {
        //noinspection ResultOfMethodCallIgnored
        file.delete();
    }
}
