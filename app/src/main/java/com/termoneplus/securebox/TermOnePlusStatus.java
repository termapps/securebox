/*
 * Copyright (C) 2018-2024 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.securebox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;


public class TermOnePlusStatus extends BroadcastReceiver {
    final static String SCHEME = "package";

    public static BroadcastReceiver register(Context context, Handler scheduler) {
        /*
         * Quote from Android 8.0 (Oreo, API level 26), "Background Execution Limits":
         * Application cannot use their manifest to register for implicit broadcasts.
         * They can still register for these broadcasts at runtime.
         */
        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme(SCHEME);
        BroadcastReceiver receiver = new TermOnePlusStatus();
        context.registerReceiver(receiver, filter, null, scheduler);
        return receiver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Uri uri = intent.getData();
        if (uri == null) return;

        if (!SCHEME.equals(uri.getScheme())) return;

        if (!Application.TERM_APPLICATION_ID.equals(uri.getSchemeSpecificPart())) return;

        String action = intent.getAction();
        if (action == null) return;

        switch (action) {
            case Intent.ACTION_PACKAGE_ADDED: {
                if (intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) break;

                // new install
                sendStatusMessage();
                break;
            }
            case Intent.ACTION_PACKAGE_REMOVED: {
                if (intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) break;
                if (!intent.getBooleanExtra(Intent.EXTRA_DATA_REMOVED, false)) break;

                // full uninstall
                sendStatusMessage();
                break;
            }
        }
    }

    private void sendStatusMessage() {
        Handler handler = MainActivity.handler;
        if (handler == null) return;
        Message msg = handler.obtainMessage(MainActivity.MSG_TERMONEPLUS_STATUS);
        handler.sendMessage(msg);
    }
}
