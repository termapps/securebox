/*
 * Copyright (C) 2017-2024 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.securebox;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.termoneplus.VirtualAccountActivity;

import java.lang.ref.WeakReference;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class MainActivity extends AppCompatActivity {

    public static final int MSG_TERMONEPLUS_STATUS = 1;

    public static Handler handler = null;
    private BroadcastReceiver terminal_appstatus;

    private View main_layout;
    private Button install_terminal;
    private Button open_terminal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        main_layout = findViewById(R.id.main_layout);

        {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        }

        install_terminal = findViewById(R.id.install_terminal);
        install_terminal.setOnClickListener(
                view -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + Application.TERM_APPLICATION_ID));
                    startActivity(intent);
                }
        );

        open_terminal = findViewById(R.id.open_terminal);
        open_terminal.setOnClickListener(
                view -> {
                    Intent intent = new Intent(Application.TERM_APPLICATION_ID + ".OPEN_NEW_WINDOW");
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivity(intent);
                }
        );
    }

    @Override
    protected void onResume() {
        super.onResume();

        onTermOnePlusStatus();
        handler = new MainHandler(this);
        terminal_appstatus = TermOnePlusStatus.register(this, null);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(terminal_appstatus);
        handler = null;
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_account) {
            Intent intent = new Intent(this, VirtualAccountActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onTermOnePlusStatus() {
        Long version = Application.appVersion(getApplicationContext(), Application.TERM_APPLICATION_ID);

        if (version == null) {
            install_terminal.setText(R.string.install_termoneplus);
            install_terminal.setVisibility(View.VISIBLE);
            open_terminal.setVisibility(View.GONE);
            return;
        }

        // Request upgrade if "TermOne Plus" does not support command interface.
        if (version < 510 /*5.1.0*/) {
            install_terminal.setText(R.string.upgrade_termoneplus);
            install_terminal.setVisibility(View.VISIBLE);
            open_terminal.setVisibility(View.GONE);
            return;
        }

        install_terminal.setVisibility(View.GONE);
        open_terminal.setVisibility(View.VISIBLE);
    }


    private static class MainHandler extends Handler {
        private final WeakReference<MainActivity> reference;

        MainHandler(MainActivity activity) {
            super(activity.getMainLooper());
            reference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = reference.get();
            if (activity == null) return;

            if (msg.what == MSG_TERMONEPLUS_STATUS) {
                activity.install_terminal.post(activity::onTermOnePlusStatus);
                return;
            }
            /*reserved for future use*/
        }
    }
}
