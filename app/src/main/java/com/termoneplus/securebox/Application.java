/*
 * Copyright (C) 2017-2024 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.securebox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.material.color.DynamicColors;
import com.termoneplus.UserAccount;
import com.termoneplus.compat.FilesCompat;
import com.termoneplus.compat.PackageInfoCompat;
import com.termoneplus.compat.PackageManagerCompat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class Application extends android.app.Application {
    public static final String TERM_APPLICATION_ID = BuildConfig.TERM_APPLICATION_ID + BuildConfig.APPLICATION_ID_SUFFIX;

    public static final Object install_lock = new Object();

    // paths
    public static File rootdir;
    public static File etcdir;
    public static File xbindir;
    public static File libdir;

    private boolean installed = false;

    public static Long appVersion(Context context, String packageName) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pkg_info = PackageManagerCompat.getPackageInfo(pm, packageName);
            return PackageInfoCompat.getVersion(pkg_info);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public void onCreate() {
        super.onCreate();

        // enable Material3 dynamic colors
        DynamicColors.applyToActivitiesIfAvailable(this);

        rootdir = getFilesDir().getParentFile();
        etcdir = new File(rootdir, "etc");
        libdir = new File(getApplicationInfo().nativeLibraryDir);
        xbindir = libdir;

        if (installed) return;
        new Thread(() -> {
            synchronized (install_lock) {
                installed = doInstall();
                install_lock.notify();
            }
            new Handler(Looper.getMainLooper()).post(this::onInstallStatus);
        }).start();
    }

    private void onInstallStatus() {
        int rid = installed
                ? R.string.app_install_success
                : R.string.app_install_failure;

        Toast toast = Toast.makeText(getApplicationContext(), rid, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 16);
        toast.show();
    }

    protected boolean doInstall() {
        boolean r;

        File piddir = new File(rootdir, "run");

        r = rootdir.setExecutable(true, false) &&
                Installer.install_directory(etcdir, false) &&
                install_public_directory(piddir);
        if (!r) return false;

        UserAccount.initialize(etcdir);

        // clean-up obsolete executable
        Installer.clean_directory(new File(rootdir, "xbin"));

        // clean-up obsolete wrapper scripts
        Installer.clean_directory(new File(Application.rootdir, "bin"));
        {
            File env = new File(Application.etcdir, "env");
            boolean ignore = env.delete();
        }

        AssetManager am = getAssets();
        try {
            String asset_path = "etc";

            //noinspection ConstantConditions
            for (String item : am.list(asset_path)) {
                r = install_configuration(am, asset_path, item, etcdir);
                if (!r) return false;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        // Note at this point xbindir == libdir
        File exe = new File(xbindir, "libcmd-ssh.so");
        if (!exe.canExecute()) {
            // Old Android (API Level < 17) - libraries are without executable bit set
            xbindir = new File(rootdir, ".x");
            r = Installer.install_directory(xbindir, false);
            if (!r) return false;

            r = Installer.copy_executable(exe, xbindir) &&
                    Installer.install_xcommands(xbindir);
            if (!r) return false;
        }

        return true;
    }

    protected final boolean install_public_directory(File dir) {
        if (!(dir.exists() || dir.mkdir())) return false;

        // always preset directory permissions
        // noinspection SetWorldReadable, SetWorldWritable
        return dir.setReadable(true, false) &&
                dir.setWritable(true, false) &&
                dir.setExecutable(true, false);
    }

    protected final boolean install_asset(AssetManager am, String asset_path, String item, File target_file) {
        int buflen = 32 * 1024; // 32k
        byte[] buf = new byte[buflen];

        try {
            String asset_item = asset_path + "/" + item;
            OutputStream os = FilesCompat.newOutputStream(target_file);
            InputStream is = am.open(asset_item, AssetManager.ACCESS_STREAMING);
            int len;
            while ((len = is.read(buf, 0, buflen)) > 0) {
                os.write(buf, 0, len);
            }
            is.close();
            os.close();

            return true;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    protected final boolean install_configuration(AssetManager am, String asset_path, String item, File target_path) {
        File target = new File(target_path, item);
        if (!install_asset(am, asset_path, item, target))
            return false;

        // always preset permissions
        // noinspection SetWorldReadable
        return target.setReadable(true, false);
    }
}
