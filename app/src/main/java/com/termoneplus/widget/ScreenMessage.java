/*
 * Copyright (C) 2019-2025 Roumen Petrov.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.termoneplus.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.Gravity;
import android.widget.Toast;


public class ScreenMessage {

    public static void show(Context context, Integer rid) {
        int offset = 0;
        try {
            try (TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize})) {
                offset = 2 * (int) styledAttributes.getDimension(0, 0);
                styledAttributes.recycle();
            }
        } catch (Exception ignore) {
        }

        Toast toast = Toast.makeText(context.getApplicationContext(), rid, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, offset);
        toast.show();
    }
}
